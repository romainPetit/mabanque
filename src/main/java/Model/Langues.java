package Model;

public enum Langues {
    FRANCAIS("fr"),
    ANGLAIS("en");

    private String abreviation;

    Langues(String abreviation) {
        this.abreviation = abreviation;
    }
    public String getAbreviation() {
        return  this.abreviation ;
    }
}
