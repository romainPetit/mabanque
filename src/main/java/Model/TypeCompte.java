package Model;

import javax.persistence.*;

@Entity
@Table(name = "typeCompte")
public class TypeCompte {
    @javax.persistence.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int Id;
    private String Libelle;
    private float Taux;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getLibelle() {
        return Libelle;
    }

    public void setLibelle(String libelle) {
        Libelle = libelle;
    }

    public float getTaux() {
        return Taux;
    }

    public void setTaux(float taux) {
        Taux = taux;
    }
}
