package Model;

import javax.persistence.*;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Set;


@Entity
@Table(name ="utilisateur")
public class Utilisateur {

    @javax.persistence.Id
    private String Login;

    private String Nom;
    private String Prenom;
    private String Email;
    private String Mdp;
    private String Telephone;
    private Date DateNaissance;
    private String Adresse;
   //private String Sexe;
    @OneToMany(mappedBy = "FkUtilisateur", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<Compte> comptes = new LinkedHashSet<Compte>();



    /*public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }*/

    public String getNom() {
        return Nom;
    }

    public void setNom(String nom) {
        Nom = nom;
    }

    public String getPrenom() {
        return Prenom;
    }

    public void setPrenom(String prenom) {
        Prenom = prenom;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getLogin() {
        return Login;
    }

    public void setLogin(String login) {
        Login = login;
    }

    public String getMdp() {
        return Mdp;
    }

    public void setMdp(String mdp) {
        Mdp = mdp;
    }

    public String getTelephone() {
        return Telephone;
    }

    public void setTelephone(String telephone) {
        Telephone = telephone;
    }

    public Date getDateNaissance() {
        return DateNaissance;
    }

    public void setDateNaissance(Date dateNaissance) {
        DateNaissance = dateNaissance;
    }

    public String getAdresse() {
        return Adresse;
    }

    public void setAdresse(String adresse) {
        Adresse = adresse;
    }

    public Set<Compte> getComptes() { return comptes; }

    public void setComptes(Set<Compte> comptes) {this.comptes = comptes;}


    public Utilisateur() {

    }

    @Override
    public String toString() {
        return "Utilisateur{" +
                "Login='" + Login + '\'' +
                ", Nom='" + Nom + '\'' +
                ", Prenom='" + Prenom + '\'' +
                ", Email='" + Email + '\'' +
                ", Mdp='" + Mdp + '\'' +
                ", Telephone='" + Telephone + '\'' +
                ", DateNaissance=" + DateNaissance +
                ", Adresse='" + Adresse + '\'' +
                ", comptes=" + comptes +
                '}';
    }
}
