package Model;

import javax.persistence.*;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Set;

@Entity
@Table(name = "compte")
public class Compte {

    @javax.persistence.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int Id;
    private float Solde;

    //private TypeCompte TypeCpt;
    private Date DateCrea;

    @OneToMany(mappedBy = "CompteConcern", cascade = CascadeType.ALL)
    private Set<Transaction> transactions = new LinkedHashSet<Transaction>();

    @ManyToOne
    @JoinColumn(name = "fkUtilisateur")
    private Utilisateur FkUtilisateur;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public float getSolde() {
        return Solde;
    }

    public void setSolde(float solde) {
        Solde = solde;
    }

    //public TypeCompte getTypeCpt() {return TypeCpt; }

    //public void setTypeCpt(TypeCompte typeCpt) {TypeCpt = typeCpt; }

    public Date getDateCrea() {
        return DateCrea;
    }

    public void setDateCrea(Date dateCrea) {
        DateCrea = dateCrea;
    }

    public Utilisateur getFkUtilisateur() {
        return FkUtilisateur;
    }

    public void setFkUtilisateur(Utilisateur romain) {
        this.FkUtilisateur = romain;
    }

    public Set<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(Set<Transaction> transactions) {
        this.transactions = transactions;
    }


}
