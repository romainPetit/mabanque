package Model;

import javax.persistence.*;
import java.util.Date;


@Entity
@Table(name ="transaction")
public class Transaction {

    @javax.persistence.Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int Id;
    private float Montant;
    private boolean Ajout;
    private Date DateTrans;
    private String Libelle;
    @ManyToOne
    @JoinColumn(name = "CompteConcern")
    private Compte CompteConcern;

    @ManyToOne
    @JoinColumn(name = "CompteCible")
    private Compte CompteCible;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public float getMontant() {
        return Montant;
    }

    public void setMontant(float montant) {
        Montant = montant;
    }

    public Date getDateTrans() {
        return DateTrans;
    }

    public void setDateTrans(Date dateTrans) {
        DateTrans = dateTrans;
    }

    public String getLibelle() {
        return Libelle;
    }

    public void setLibelle(String libelle) {
        Libelle = libelle;
    }

    public boolean isAjout() {
        return Ajout;
    }

    public void setAjout(boolean ajout) {
        Ajout = ajout;
    }

    public Compte getCompteConcern() {
        return CompteConcern;
    }

    public void setCompteConcern(Compte compteConcern) {
        CompteConcern = compteConcern;
    }

    public Compte getCompteCible() {
        return CompteCible;
    }

    public void setCompteCible(Compte compteCible) {
        CompteCible = compteCible;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "Id=" + Id +
                ", Montant=" + Montant +
                ", Ajout=" + Ajout +
                ", DateTrans=" + DateTrans +
                ", Libelle='" + Libelle + '\'' +
                ", CompteConcern=" + CompteConcern +
                ", CompteCible=" + CompteCible +
                '}';
    }
}
