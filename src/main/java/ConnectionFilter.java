import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import java.util.logging.LogRecord;

@WebFilter("/banque/*")
public class ConnectionFilter implements Filter {
    public  void doFilter(ServletRequest req, ServletResponse resp,
                          FilterChain filterChain)
            throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;
        HttpSession session = request.getSession(false);
        //String loginURI = ServletHelper.getServletUrl(ServletHelper.SERVLET_LOGIN, req);
        String loginURI = request.getContextPath() + "/banque/login";

        boolean loggedIn = session != null && session.getAttribute("utilisateurCon") != null;
        boolean loginRequest = request.getRequestURI().equals(loginURI);

        if (loggedIn || loginRequest) {
            filterChain.doFilter(request, response);
        } else {
            response.sendRedirect(loginURI);
        }
    }


}
