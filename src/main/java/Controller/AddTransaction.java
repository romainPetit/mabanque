package Controller;

import Model.Compte;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/banque/addTransaction")
public class AddTransaction extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getSession().getAttribute("utilisateurCon");
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/WEB-INF/transaction.jsp");
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String montant = req.getParameter("montant");
        String libelle = req.getParameter("libelle");
        String idCompteDebiter = req.getParameter("compteDebiter");
        String idCompteCrediter = req.getParameter("compteCrediter");

        System.out.println("montant : " + montant + ", libelle : "+ libelle+ " , id compteDebiter : " + idCompteDebiter+ " , id compteCrediter : "+ idCompteCrediter);

        ServletContext context = this.getServletContext();
        RequestDispatcher dispatcher = context.getRequestDispatcher("/rest/transaction");
        dispatcher.forward(req, resp);

    }
}
