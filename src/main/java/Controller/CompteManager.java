package Controller;

import Model.Compte;
import Model.Utilisateur;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.Date;
import java.util.List;

public class CompteManager extends BaseManager {

    public static List<Compte> loadCompteByUtilisateur(String login){
        try{
            EntityManager em = getEntityManager();
            TypedQuery<Compte> query = em.createQuery(
                    "SELECT c from Compte c where c.FkUtilisateur ='" + login + "' order by c.id " , Compte.class);
            List<Compte> comptes = query.getResultList();

        for(Compte iter:comptes){
            System.out.println(iter.getId());
        }

            return comptes;

        }catch (Exception e){
            return null;
        }

    }

    public static Compte loadCompteById(String idCompte){
        try{
            EntityManager em = getEntityManager();
            TypedQuery<Compte> query = em.createQuery(
                    "SELECT c from Compte c where c.Id ='" + idCompte + "'" , Compte.class);
            Compte compte = query.getSingleResult();

        /*for(Compte iter:util.getComptes()){
            System.out.println(iter);
        }*/

            return compte;

        }catch (Exception e){
            return null;
        }

    }

    public static void updateSolde(String idCompte, float solde){
        Compte compte = loadCompteById(idCompte);
        compte.setSolde(solde);

        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        em.merge(compte);
        em.getTransaction().commit();
    }

    public static void addCompte(Utilisateur util, float solde){
        Compte compte = new Compte();
        compte.setSolde(solde);
        compte.setDateCrea(new Date());
        compte.setFkUtilisateur(util);

        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        em.persist(compte);
        em.getTransaction().commit();
    }
}
