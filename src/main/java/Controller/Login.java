package Controller;

import Model.Langues;
import Model.Utilisateur;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;


public class Login extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/WEB-INF/home.jsp");

        dispatcher.forward(req, resp);
        String language = req.getParameter("language");
        //System.out.println("language get : "+language);


        /*List<Langues> langues = new ArrayList<Langues>();
        langues.add(Langues.ANGLAIS);
        langues.add(Langues.FRANCAIS);

        EnumSet<Langues> allLangues = EnumSet.allOf( Langues.class );
        System.out.println( "allLangues : " + allLangues );
        req.setAttribute("langues", allLangues);

*/



        //resp.getWriter().println("hello world");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter out = resp.getWriter();

        String login = req.getParameter("login");
        String mdp = req.getParameter("password");
        String message = null;

        String language = req.getParameter("language");
        //System.out.println("language : "+language);

        Utilisateur util = new Utilisateur();

        util = UtilisateurManager.loadUtilisateurByLoginAndPassword(login, mdp);




        if(util != null){
            //System.out.println("utilisateur : "+ util.getPrenom());
            req.getSession().setAttribute("utilisateurCon", util);
            //getServletContext().getRequestDispatcher("/monEspace").forward(req,resp);
            req.getSession().setMaxInactiveInterval(2*60);
            //req.setAttribute("message", message);
            resp.sendRedirect(req.getContextPath() + "/banque/monEspace");


        }
        else{

            message = "Utilisateur ou mot de passe incorrect";
            req.setAttribute("message", message);

            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/WEB-INF/home.jsp");
            dispatcher.forward(req, resp);


            /*out.println("<script type=\"text/javascript\">");
            out.println("alert('User or password incorrect');");
            out.println("location='login';");
            out.println("</script>");*/

        }


    }
}
