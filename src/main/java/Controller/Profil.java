package Controller;

import Model.Utilisateur;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/banque/monProfil")
public class Profil extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/WEB-INF/profil.jsp");
        dispatcher.forward(req, resp);

        req.getSession().getAttribute("utilisateurCon");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Validation valid = new Validation();
        String oldMdp = req.getParameter("currentPassword");
        String newMdp = req.getParameter("newPassword");
        String confirmMdp = req.getParameter("newConfirmPassword");
        String errorMsg = null;
        String validMsg = null;
        List<String> infosMdp = new ArrayList<>();

        System.out.println("odlMdp : " + oldMdp + "\n newMdp : " + newMdp + " \n confirmMdp : " + confirmMdp);
        String id = req.getParameter("id");
        Utilisateur util = UtilisateurManager.loadClientByLogin(id);

        System.out.println("1 : "+oldMdp);
        System.out.println("2 : "+util.getMdp());


        if(oldMdp.equals(util.getMdp())){
            if (newMdp.equals(confirmMdp)) {
                if (valid.validerMdp(newMdp)) {
                    System.out.println("marche");
                    UtilisateurManager.updateMdpFromLogin(id, newMdp);
                    validMsg = "mot de passe modifié !";
                    req.setAttribute("validMsg", validMsg);

                }
                else {
                    infosMdp.add("- Le mot de passe doit contenir au moins 8 caractères,");
                    infosMdp.add("- Le mot de passe doit contenir au moins une majuscule,");
                    infosMdp.add("- Le mot de passe doit contenir au moins un caractère spécial,");
                    infosMdp.add("- Le mot de passe ne doit pas contenir de lettre avec accent.");

                    req.setAttribute("infosMdp", infosMdp);

                    RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/WEB-INF/profil.jsp");
                    dispatcher.forward(req, resp);
                }
            } else {
                errorMsg = "le nouveau mdp doit etre identique";
                System.out.println("le nouveau mdp doit etre identique");
                req.setAttribute("errorMsg", errorMsg);

                RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/WEB-INF/profil.jsp");
                dispatcher.forward(req, resp);
            }
        }
        else{
            errorMsg = "Le mot de passe de l'utilisateur n'est pas le bon !";
            System.out.println("Le mot de passe de l'utilisateur n'est pas le bon !");
            req.setAttribute("errorMsg", errorMsg);

            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/WEB-INF/profil.jsp");
            dispatcher.forward(req, resp);
        }

        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/WEB-INF/profil.jsp");
        dispatcher.forward(req, resp);


    }
}
