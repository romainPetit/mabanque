package Controller;

import Model.Compte;
import Model.Transaction;
import Model.Utilisateur;
import jdk.nashorn.internal.scripts.JS;
import netscape.javascript.JSObject;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.rmi.CORBA.Util;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;


public class Detail extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String id = req.getParameter("id");
        OkHttpClient client = new OkHttpClient();

        // Utilisation API REST, recuperation du solde du compte par la methode GET
        Request request = new Request.Builder()
                .url("http://localhost:8080/rest/compte/"+id)
                .build();

        Response response = client.newCall(request).execute();
        String donnee = response.body().string();

        JSONObject obj = new JSONObject(donnee);
        String sold = obj.getString("solde");

        System.out.println("==========> obj : "+ obj);

        req.setAttribute("soldeCompte", sold);



        Compte compte = CompteManager.loadCompteById(id);

        req.setAttribute("compte", compte);

        List<Transaction> transactionsCompte = new ArrayList<>();

        Request requestTrans = new Request.Builder()
                .url("http://localhost:8080/rest/transaction/"+id)
                .build();

        Response responseTrans = client.newCall(requestTrans).execute();
        String donneeTrans = responseTrans.body().string();

        JSONArray objTrans = new JSONArray(donneeTrans);

        System.out.println("longueur JSONArray = " + objTrans.length());

        for(int i = 0 ; i < objTrans.length(); i++){
            Transaction transaction = new Transaction();

            Compte compteConcern = new Compte();
            Compte compteCible = new Compte();

            // Utilisateur du compte Concerné
            Utilisateur utilConcern = UtilisateurManager.loadClientByLogin(objTrans.getJSONObject(i).getJSONObject("compteConcern").getString("loginCompteFk"));


            // Utilisateur du compte cible
            Utilisateur utilCible = UtilisateurManager.loadClientByLogin(objTrans.getJSONObject(i).getJSONObject("compteCible").getString("loginCompteFk"));

            //Compte Concerné
            compteConcern.setId(objTrans.getJSONObject(i).getJSONObject("compteConcern").getInt("idCompte"));
            compteConcern.setSolde(objTrans.getJSONObject(i).getJSONObject("compteConcern").getFloat("solde"));
            compteConcern.setFkUtilisateur(utilConcern);

            //Compte Cible
            compteCible.setId(objTrans.getJSONObject(i).getJSONObject("compteCible").getInt("idCompte"));
            compteCible.setSolde(objTrans.getJSONObject(i).getJSONObject("compteCible").getFloat("solde"));
            compteCible.setFkUtilisateur(utilCible);


            //Transaction pour le compte Concerné
            transaction.setId(objTrans.getJSONObject(i).getInt("id"));
            transaction.setLibelle(objTrans.getJSONObject(i).getString("libelle"));
            transaction.setMontant(objTrans.getJSONObject(i).getFloat("montant"));
            transaction.setAjout(objTrans.getJSONObject(i).getBoolean("isAjout"));

            transaction.setCompteConcern(compteConcern);
            transaction.setCompteCible(compteCible);

            // erreur de parse pour la date a demander au prof (peut etre bon!)
            String dateTrans = objTrans.getJSONObject(i).getString("date");
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.FRANCE);
            try {
                Date result = dateFormat.parse(dateTrans);
                transaction.setDateTrans(result);
            } catch (ParseException pe) {
                transaction.setDateTrans(new Date());
                pe.printStackTrace();
            }

            System.out.println("Date transaction : "+transaction.getDateTrans());
            System.out.println("===========================================TEST==============================================");
            System.out.println("transaction object = "+ transaction.toString());

            transactionsCompte.add(transaction);

        }
        System.out.println("TOUTE MES TRANSACTIONS : ");
        for(int t = 0; t<transactionsCompte.size(); t++){
            System.out.println("Transaction "+t +" : "+ transactionsCompte.get(t));
        }



        //System.out.println(transactions.size());
        //List<Transaction> transactions = TransactionManager.loadTransactionDebitCreditByIdCompte(id);
        req.setAttribute("transactions", transactionsCompte);


        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/WEB-INF/detail.jsp");
        dispatcher.forward(req, resp);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }

}
