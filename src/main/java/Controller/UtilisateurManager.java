package Controller;

import Model.Compte;
import Model.Utilisateur;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import java.util.logging.LogManager;
import java.util.logging.Logger;


public class UtilisateurManager extends BaseManager{

    public static void saveUtilisateur(Utilisateur utilisateur){
        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        em.persist(utilisateur);
        em.getTransaction().commit();
    }

    public static Utilisateur loadClientByLogin(String utilisateurLogin){
        EntityManager em = getEntityManager();
        Utilisateur util = em.find(Utilisateur.class, utilisateurLogin);
        //System.out.println(util.toString());

        return util;
    }

    public static Utilisateur loadUtilisateurByLoginAndPassword(String login, String password){
        try{
            EntityManager em = getEntityManager();
            TypedQuery<Utilisateur> query = em.createQuery(
                    "SELECT c from Utilisateur c where c.Login='" + login + "' and c.Mdp= '"+ password + "'" , Utilisateur.class);
            Utilisateur util = query.getSingleResult();

        /*for(Compte iter:util.getComptes()){
            System.out.println(iter);
        }*/

            return util;

        }catch (Exception e){
            return null;
        }

    }

    public static void purgeTable(){
        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        em.createQuery("delete from Transaction").executeUpdate();
        em.createQuery("delete from Compte").executeUpdate();
        em.createQuery("delete from Utilisateur").executeUpdate();
        em.getTransaction().commit();
    }
    public static void deleteUtil(Utilisateur util){
        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        em.remove(util);
        em.getTransaction().commit();

    }

    public static void updateMdpFromLogin(String login, String mdp){
        Utilisateur utilisateur = UtilisateurManager.loadClientByLogin(login);
        utilisateur.setMdp(mdp);

        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        //em.createQuery("UPDATE Utilisateur u set u.Mdp = '"+mdp+"' where u.Login = '"+login+"'");
        em.merge(utilisateur);
        em.getTransaction().commit();

    }


}
