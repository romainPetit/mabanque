package Controller;

import Model.Utilisateur;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/banque/nouveauCompte")
public class NouveauCompte extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/WEB-INF/nouveauCompte.jsp");
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Utilisateur util = (Utilisateur) req.getSession().getAttribute("utilisateurCon");
        Float solde = Float.parseFloat(req.getParameter("solde"));
        CompteManager.addCompte(util, solde);
        resp.sendRedirect(req.getContextPath() + "/banque/monEspace");
    }
}
