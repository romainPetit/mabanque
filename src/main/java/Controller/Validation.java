package Controller;

import com.sun.org.apache.xpath.internal.operations.Bool;

import java.util.regex.Pattern;

public class Validation {

    public Boolean validerMdp(String newMdp){
        boolean valide = false;
        if(isGoodLength(newMdp)){
            if(isMajInMdp(newMdp)){
                if(isSpecialCarac(newMdp)){
                    if(isNoAccent(newMdp)){
                        valide=true;
                    }
                }
            }
        }
        return valide;
    }

    public Boolean isGoodLength(String newMdp){
        Boolean valid = false;

        if(newMdp.length() >= 8){
            valid = true;
        }
        return valid;
    }

    public Boolean isMajInMdp(String newMdp){
        Boolean valid = false;
        for (int i=0; i < newMdp.length(); i++){
            char c = newMdp.charAt(i);
            if(Character.isUpperCase(c)){
                valid = true;
            }
        }
        return valid;
    }

    public Boolean isSpecialCarac(String newMdp){
        Boolean valid = false;
        String specialChars = "/*!@#$%^&*()\"{}_[]|\\?/<>,.";

        for (int i=0; i < newMdp.length(); i++){
            if (specialChars.contains(newMdp.substring(i, i+1))) {
                valid = true;
            }
        }
        return valid;
    }

    public Boolean isNoAccent(String newMdp){
        Boolean valid = false;
        if (!Pattern.matches(".*[àâáäãåāéèêëęėēÿûùüúūîïìíįīôöòóõøōçćčñńÀÂÁÄÃÅĀÉÈÊËĘĖĒÛÙÜÚŪÎÏÌÍĮĪÔÖÒÒÓÕØŌÇĆČÑŃ].*", newMdp)){
            valid = true;
        }

        return valid;
    }


}
