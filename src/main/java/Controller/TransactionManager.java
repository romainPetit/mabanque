package Controller;

import Model.Compte;
import Model.Transaction;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.Date;
import java.util.List;

public class TransactionManager extends BaseManager {
    public static List<Transaction> loadTransactionDebitCreditByIdCompte(String id) {
        try {
            EntityManager em = getEntityManager();
            TypedQuery<Transaction> query = em.createQuery(
                    "SELECT t from Transaction t where t.CompteConcern ='" + id + "'", Transaction.class);
            List<Transaction> transactions = query.getResultList();

        for(Transaction iter:transactions){
            System.out.println(iter.toString());
        }

            return transactions;

        } catch (Exception e) {
            return null;
        }
    }

    public static void addTransaction(Transaction trans){
        EntityManager em = getEntityManager();
        em.getTransaction().begin();
        em.persist(trans);
        em.getTransaction().commit();
    }
}
