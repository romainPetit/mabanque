package REST;

import Controller.CompteManager;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/rest/compte/*")

public class Compte extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String pathInfo = req.getPathInfo();
        String[] splits = pathInfo.split("/");
        System.out.println("pathInfo split : "+splits[1]);
        String id = splits[1];

        Model.Compte monCompte = CompteManager.loadCompteById(id);
        String solde = ""+ monCompte.getSolde();

        JSONObject json = new JSONObject();
        json.put("idCompte", id);
        json.put("solde", solde);




        resp.getWriter().println(json);
        //resp.sendRedirect(req.getContextPath() + "/banque/detailCompte?id="+id);
        //RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/WEB-INF/detail.jsp");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
