package REST;

import Controller.CompteManager;
import Controller.TransactionManager;
import Model.Compte;
import org.json.JSONArray;
import org.json.JSONObject;


import javax.json.Json;
import javax.json.JsonArray;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.List;



@WebServlet("/rest/transaction/*")
public class Transaction extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String pathInfo = req.getPathInfo();
        String[] splits = pathInfo.split("/");
        System.out.println("pathInfo split : "+splits[1]);
        String id = splits[1];

        List<Model.Transaction> transactions = TransactionManager.loadTransactionDebitCreditByIdCompte(id);

        JSONArray jsonA = new JSONArray();
        for(Model.Transaction trans : transactions){
            jsonA.put(new JSONObject()
                    .put("id", trans.getId())
                    .put("libelle", trans.getLibelle())
                    .put("montant", trans.getMontant())
                    .put("date", trans.getDateTrans())
                    .put("compteConcern", new JSONObject()
                            .put("idCompte", trans.getCompteConcern().getId())
                            .put("solde", trans.getCompteConcern().getSolde())
                            .put("dateCrea", trans.getCompteConcern().getDateCrea().toString())
                            .put("loginCompteFk", trans.getCompteConcern().getFkUtilisateur().getLogin()))
                    .put("compteCible", new JSONObject()
                            .put("idCompte", trans.getCompteCible().getId())
                            .put("solde", trans.getCompteCible().getSolde())
                            .put("dateCrea", trans.getCompteCible().getDateCrea().toString())
                            .put("loginCompteFk", trans.getCompteCible().getFkUtilisateur().getLogin()))
                    .put("isAjout", trans.isAjout()));
        }
        resp.getWriter().println(jsonA);



    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        /*resp.getWriter().println("123");

        req.getSession().getAttribute("utilisateurCon");
        Compte cpt1 = CompteManager.loadCompteById("1");
        Compte cpt2 = CompteManager.loadCompteById("2");*/

        Date date = new Date();
        String montant = req.getParameter("montant");
        String libelle = req.getParameter("libelle");
        String idCompteDebiter = req.getParameter("compteDebiter");
        String idCompteCrediter = req.getParameter("compteCrediter");

        System.out.println(" --------------------------> montant : " + montant + ", libelle : "+ libelle+ " , id compteDebiter : " + idCompteDebiter+ " , id compteCrediter : "+ idCompteCrediter);

        Float m = Float.parseFloat(montant);
        Compte cptDebiter = CompteManager.loadCompteById(idCompteDebiter);
        Compte cptCrediter = CompteManager.loadCompteById(idCompteCrediter);

        Model.Transaction transCredit = new Model.Transaction();
        transCredit.setMontant(m);
        transCredit.setAjout(true);
        transCredit.setDateTrans(date);
        transCredit.setLibelle(libelle);
        transCredit.setCompteConcern(cptCrediter);
        transCredit.setCompteCible(cptDebiter);

        Model.Transaction transDebit = new Model.Transaction();
        transDebit.setMontant(m);
        transDebit.setAjout(false);
        transDebit.setDateTrans(date);
        transDebit.setLibelle(libelle);
        transDebit.setCompteConcern(cptDebiter);
        transDebit.setCompteCible(cptCrediter);


        try {
            TransactionManager.addTransaction(transCredit);
            TransactionManager.addTransaction(transDebit);

            float soldeCptCredit = transCredit.getCompteConcern().getSolde() + m;
            CompteManager.updateSolde(transCredit.getCompteConcern().getId()+"",soldeCptCredit);

            float soldeCptDebit = transCredit.getCompteCible().getSolde() - m;
            CompteManager.updateSolde(transCredit.getCompteCible().getId()+"",soldeCptDebit);

            resp.sendRedirect(req.getContextPath() + "/banque/detailCompte?id="+ idCompteCrediter);

        } catch (Exception e) {
            resp.getWriter().println("error");
        }
    }

}
