package test;

import Controller.Validation;
import com.sun.org.apache.xpath.internal.operations.Bool;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.*;

class ValidationTest {
    public String oldMdp = "azert";
    public String mdpJuste = "Azertyuiop1!";
    public String mdp8carac = "Azer1!";
    public String mdpMaj =" azertyuiop1!";
    public String mdpAccent = "Azérty(uiop3";
    Validation valid = new Validation();

    @Test
    public void mdpTailleJuste(){
        assertTrue(valid.isGoodLength(mdpJuste), "taille mot de passe valide !" );
    }

    @Test void mdpTailleWrong(){
        assertFalse(valid.isGoodLength(oldMdp), " le mot de passe doit être suppérieur à 8 caractères !");
    }

    @Test void mdpMajJuste(){
        assertTrue(valid.isGoodLength(mdpJuste), " le mot de passe contient au moins une majuscule !");
    }

    @Test void mdpMajWrong(){
        assertFalse(valid.isMajInMdp(oldMdp), " le mot de passe doit contenir au moins une majuscule !");
    }

    @Test void mdpCaracSpeJuste(){
        assertTrue(valid.isSpecialCarac(mdpAccent), "le mot de passe contient bien au moins un caractère spécial !");
    }

    @Test void mdpCaracSpeWrong(){
        assertFalse(valid.isSpecialCarac(oldMdp), " le mot de passe doit contenir au moins un caractère spécial !");
    }

    @Test void mdpNoAccentJuste(){
        assertTrue(valid.isNoAccent(mdpJuste), "le mot de passe ne contient pas d'accent !");
    }

    @Test void mdpNoAccentWrong(){
        assertFalse(valid.isNoAccent(mdpAccent), "le mot de passe ne doit pas contenir d'accent !");
    }

    @Test void fncValiderJuste(){
        assertTrue(valid.validerMdp(mdpJuste), "le mdp est juste");
    }

    @Test void fncValiderWrong(){
        assertFalse(valid.validerMdp(mdp8carac), "le mdp est faux");
    }

}