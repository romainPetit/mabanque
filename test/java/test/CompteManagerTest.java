package test;

import Controller.CompteManager;
import Controller.UtilisateurManager;
import Model.Compte;
import Model.Utilisateur;
import org.junit.jupiter.api.Test;

import java.util.List;

class CompteManagerTest {

    @Test
    public void loadComptes(){

        List<Compte> comptes = CompteManager.loadCompteByUtilisateur("romP");
        for (Compte compte : comptes) {
           System.out.println(compte.getFkUtilisateur());
       }

        //System.out.println(comptes.toString());
    }

    @Test
    public void AddCompte(){
        Utilisateur util = UtilisateurManager.loadClientByLogin("rom");
        Float solde = Float.parseFloat("123");
        CompteManager.addCompte(util, solde);
    }
}