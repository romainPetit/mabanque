package test;

import Controller.UtilisateurManager;
import Model.Compte;
import Model.Transaction;
import Model.Utilisateur;
import org.junit.jupiter.api.Test;

import java.util.Date;
import java.util.LinkedHashSet;

class UtilisateurManagerTest {

    //Logger logger = LogManager.getLogger(LoginServlet.class);

    //logger.error("Client not found" + login + " "+ password);

    /*@BeforeAll
    public void dropTable(){
        UtilisateurManager.purgeTable();
        Logger.info
    }*/

    @Test
    public void supprimerDonnees(){
        UtilisateurManager.purgeTable();
    }

    @Test
    public void loadUtil(){
        UtilisateurManager.loadUtilisateurByLoginAndPassword("romP", "motdepasse");
    }

    Utilisateur util = new Utilisateur();
    LinkedHashSet<Compte> comptes = new LinkedHashSet<Compte>();
    LinkedHashSet<Transaction> transactionsCompte1 = new LinkedHashSet<Transaction>();
    LinkedHashSet<Transaction> transactionsCompte2 = new LinkedHashSet<Transaction>();
    LinkedHashSet<Transaction> transactionsCompte3 = new LinkedHashSet<Transaction>();

    Compte compte1 = new Compte();
    Compte compte2 = new Compte();
    Compte compte3 = new Compte();
    @Test
    public void ajoutUtilisateur(){

        //util.setId(1);
        util.setPrenom("Romain");
        util.setNom("Petit");
        util.setEmail("romain@gmail.com");
        util.setLogin("rom");
        util.setMdp("azert");
        util.setTelephone("06 01 73 46 65");
        util.setDateNaissance(new Date());
        util.setAdresse("14 rue de balambits");


        compte1.setSolde(2000);
        compte1.setDateCrea(new Date());
        compte1.setFkUtilisateur(util);


        compte2.setSolde(89000);
        compte2.setDateCrea(new Date());
        compte2.setFkUtilisateur(util);


        compte3.setSolde(1698);
        compte3.setDateCrea(new Date());
        compte3.setFkUtilisateur(util);


        comptes.add(compte1);
        comptes.add(compte2);
        comptes.add(compte3);



        // ajout (+200€) pour le compte 1  -> -200 pour le compte2
        Transaction trans1 = new Transaction();
        trans1.setCompteConcern(compte1);
        trans1.setAjout(true);
        trans1.setLibelle("achat AirPod 2");
        trans1.setCompteCible(compte2);
        trans1.setDateTrans(new Date());
        trans1.setMontant(200);

        // retrait (-200€) pour le compte 2  -> +200 pour le compte1
        Transaction trans2 = new Transaction();
        trans2.setCompteConcern(compte2);
        trans2.setAjout(false);
        trans2.setLibelle("achat AirPod 2");
        trans2.setCompteCible(compte1);
        trans2.setDateTrans(new Date());
        trans2.setMontant(200);


        // retrait (-180€) pour le compte 1 -> + 180 pour le compte 3
        Transaction trans3 = new Transaction();
        trans3.setCompteConcern(compte1);
        trans3.setAjout(false);
        trans3.setLibelle("achat nike air max 97");
        trans3.setCompteCible(compte3);
        trans3.setDateTrans(new Date());
        trans3.setMontant(180);

        // ajout (+180€) pour le compte 3 -> - 180 pour le compte 1
        Transaction trans4 = new Transaction();
        trans4.setCompteConcern(compte3);
        trans4.setAjout(true);
        trans4.setLibelle("achat nike air max 97");
        trans4.setCompteCible(compte1);
        trans4.setDateTrans(new Date());
        trans4.setMontant(180);


        // retrait (-1800€) pour le compte 3 -> ajout (+1800€) pour le compte 2
        Transaction trans5 = new Transaction();
        trans5.setCompteConcern(compte3);
        trans5.setAjout(false);
        trans5.setLibelle("achat macbook");
        trans5.setCompteCible(compte2);
        trans5.setDateTrans(new Date());
        trans5.setMontant(1800);

        // ajout (+1800€) pour le compte 2 -> retrait (-1800€) pour le compte 3
        Transaction trans6 = new Transaction();
        trans6.setCompteConcern(compte2);
        trans6.setAjout(true);
        trans6.setLibelle("achat macbook");
        trans6.setCompteCible(compte3);
        trans6.setDateTrans(new Date());
        trans6.setMontant(1800);


        transactionsCompte1.add(trans1);
        transactionsCompte1.add(trans3);


        transactionsCompte2.add(trans2);
        transactionsCompte2.add(trans6);

        transactionsCompte3.add(trans4);
        transactionsCompte3.add(trans5);


        compte1.setTransactions(transactionsCompte1);
        compte2.setTransactions(transactionsCompte2);
        compte3.setTransactions(transactionsCompte3);

        util.setComptes(comptes);
        UtilisateurManager.saveUtilisateur(util);

    }

    @Test
    public void updateUtil(){
        UtilisateurManager.updateMdpFromLogin("rom", "Azertyuiop1@");

    }

    @Test
    public void getUtilById(){
         Utilisateur util = new Utilisateur();
         util = UtilisateurManager.loadUtilisateurByLoginAndPassword("rom", "azert");
         System.out.println(util);

    }
}