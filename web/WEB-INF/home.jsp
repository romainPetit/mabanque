<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="login page">
    <meta name="author" content="Romain P">

    <title><fmt:message key="connexion"/></title>

    <!-- Bootstrap core CSS-->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link href="../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Custom styles for this template-->
    <link href="../css/sb-admin.css" rel="stylesheet">

</head>
<body class="bg-dark">
<div class="container">
    <div class="card card-login mx-auto mt-5">
        <div class="card-header"><fmt:message key="bienvenue"/></div>
        <div class="card-body">
            <h5 style="color: red"><c:out value="${message}"/></h5>
            <form action="login" method="post">
                <div class="form-group">
                    <div class="form-label-group">
                        <input type="text" id="inputEmail" name="login" class="form-control" placeholder="Adresse Email" required="required" autofocus="autofocus">
                        <label for="inputEmail"><fmt:message key="utilisateurlog"/></label>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-label-group">
                        <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password" required="required">
                        <label for="inputPassword"><fmt:message key="mdp"/></label>
                    </div>
                </div>
                <input type="submit" class="btn btn-primary btn-block" value="<fmt:message key="connexion"/>">
            </form>
            <div class="text-center">
                <%@include file="selectlangue.jsp"%>
            </div>
        </div>
    </div>
</div>
</body>
</html>