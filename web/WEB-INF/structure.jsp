<%--
  Created by IntelliJ IDEA.
  User: romainpetit
  Date: 22/10/2018
  Time: 21:27
  To change this template use File | Settings | File Templates.
--%>
<nav class="navbar navbar-expand navbar-dark bg-dark static-top">

    <a class="navbar-brand mr-1" href="/banque/monEspace"><fmt:message key="maBanque"/> </a>

    <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
        <i class="fas fa-bars"></i>
    </button>

    <!-- Navbar -->
    <ul class="navbar-nav ml-auto ml-md-0">
        <li class="nav-item dropdown no-arrow mx-1">
        </li>
        <li class="nav-item dropdown no-arrow mx-1">
        </li>
        <li class="nav-item dropdown no-arrow">
            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-user-circle fa-fw"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="/banque/monProfil"><fmt:message key="profil"/> </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="/banque/deconnexion" data-toggle="modal" data-target="#logoutModal"><fmt:message key="deconnexion"/> </a>
            </div>
        </li>
    </ul>

</nav>
<div id="wrapper">

    <!-- Sidebar -->
    <ul class="sidebar navbar-nav">
        <li class="nav-item">
            <a class="nav-link" href="/banque/monEspace">
                <i class="fas fa-fw fa-tachometer-alt"></i>
                <span><fmt:message key="maBanque"/> </span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/banque/addTransaction">
                <i class="fas fa-fw fa-tachometer-alt"></i>
                <span>Transaction</span>
            </a>
        </li>

    </ul>

