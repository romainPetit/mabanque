<%--
  Created by IntelliJ IDEA.
  User: romainpetit
  Date: 02/11/2018
  Time: 09:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page pageEncoding="UTF-8" %>
<html>
<head>
    <title><fmt:message key="nouveauCompte"/> </title>

    <%@include file="linkBootsrap.jsp"%>

</head>
<body>
<%@include file="structure.jsp"%>

<div id="content-wrapper">

    <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="/banque/monEspace"><fmt:message key="maBanque"/></a>
            </li>
            <li class="breadcrumb-item active"><fmt:message key="ajouterCompte"/></li>
        </ol>

        <!-- Page Content -->

        <form action="nouveauCompte" method="post">
            <div class="form-group">
                <label for="inputSolde">Solde</label>
                <input type="number" step="0.01" class="form-control" id="inputSolde" name="solde" aria-describedby="libelleHelp" placeholder="Ajoutez un montant">
                <%--<small id="libelleHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>--%>
            </div>
            <button type="submit" class="btn btn-primary"><fmt:message key="creer"/> </button>
        </form>


        <!-- /.container-fluid -->

        <%@include file="footer.jsp"%>

    </div>



    <%--<div>
        <div>
            <c:forEach items="${ utilisateurCon.getComptes() }" var="compte" varStatus="status">
                <p>N°<c:out value="${ compte.getId() }" /> : <c:out value="${ compte.getSolde() }" /> €</p>
                <form action="detailCompte">
                    <input type="hidden" name="id" value="${compte.getId()}" />
                    <input type="submit" name="details" value="<fmt:message key="detail"/>" />
                </form>
            </c:forEach>
        </div>
    </div>--%>



</body>
</html>
