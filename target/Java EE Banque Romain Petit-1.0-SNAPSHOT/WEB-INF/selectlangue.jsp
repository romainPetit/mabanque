<%--
  Created by IntelliJ IDEA.
  User: romainpetit
  Date: 18/10/2018
  Time: 14:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<form>
    <select id="language" name="language" onchange="submit()">
        <option value="fr" ${language == 'fr' ? 'selected' : ''}>Français</option>
        <option value="en" ${language == 'en' ? 'selected' : ''}>English</option>
    </select>
</form>