<%--
  Created by IntelliJ IDEA.
  User: romainpetit
  Date: 27/10/2018
  Time: 16:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page pageEncoding="UTF-8" %>
<html>
<head>
    <title><fmt:message key="monProfil"/> </title>
    <%@include file="linkBootsrap.jsp"%>
</head>
<body>
<%@include file="structure.jsp"%>
<div class="container">
    <div class="row">
        <div class="col-md-6  offset-md-0  toppad" >
            <div class="card">
                <div class="card-body">
                    <h2 class="card-title">${ utilisateurCon.getPrenom() } ${ utilisateurCon.getNom() }</h2>
                    <table class="table table-user-information ">
                        <tbody>
                        <tr>
                            <td>Email</td>
                            <td>${ utilisateurCon.getEmail() }</td>
                        </tr>
                        <tr>
                            <td><fmt:message key="telephone"/> </td>
                            <td>${ utilisateurCon.getTelephone() }</td>
                        </tr>
                        <tr>
                            <td><fmt:message key="adresse"/> </td>
                            <td>${ utilisateurCon.getAdresse() }</td>
                        </tr>
                        <tr>
                            <td><fmt:message key="dateNaissance"/> </td>
                            <td>${ utilisateurCon.getDateNaissance() }</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <%@include file="changermdp.jsp"%>
    </div>
</div>
<%@include file="footer.jsp"%>
</body>
</html>
