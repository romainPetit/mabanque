<%--
  Created by IntelliJ IDEA.
  User: romainpetit
  Date: 19/10/2018
  Time: 11:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title><fmt:message key="comptes"/> </title>

    <%@include file="linkBootsrap.jsp"%>
</head>
<body>
<%@include file="structure.jsp"%>

<div id="content-wrapper">

    <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="/banque/monEspace"><fmt:message key="maBanque"/></a>
            </li>
            <li class="breadcrumb-item active"><fmt:message key="detailCompteNum"/> N°<c:out value="${ compte.getId() }" /></li>
        </ol>

        <div class="card mb-3">
            <div class="card-header">
                <p><fmt:message key="compte"/> N°<c:out value="${ compte.getId() }" /> : <c:out value="${ soldeCompte }" /> €</p>
            </div>

            <c:if test="${transactions.size() >= 1}">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th></th>
                                <th>LIBELLE</th>
                                <th><fmt:message key="MONTANT"/></th>
                                <th>DATE</th>
                                <th><fmt:message key="VERSCOMPTE"/></th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th></th>
                                <th>LIBELLE</th>
                                <th><fmt:message key="MONTANT"/></th>
                                <th>DATE</th>
                                <th><fmt:message key="VERSCOMPTE"/></th>
                            </tr>
                            </tfoot>
                            <tbody>
                            <c:forEach items="${ transactions }" var="transaction" varStatus="status">
                                <tr <c:if test="${!transaction.isAjout()}">
                                    class="table-danger"
                                </c:if>>
                                    <td>${transaction.getId()}</td>
                                    <td>${transaction.getLibelle()}</td>
                                    <td>
                                        <c:if test="${!transaction.isAjout()}">
                                            - ${transaction.getMontant()} €
                                        </c:if>
                                        <c:if test="${transaction.isAjout()}">
                                            + ${transaction.getMontant()} €
                                        </c:if>
                                    </td>
                                    <td>${transaction.getDateTrans()}</td>
                                    <td>${transaction.getCompteCible().getId()}</td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
            </c:if>
            <c:if test="${transactions.size() <= 0}">
                <div>
                    <p><fmt:message key="pasTransaction"/></p>
                </div>
            </c:if>

        </div>
    </div>
    <%@include file="footer.jsp"%>
</body>
</html>
