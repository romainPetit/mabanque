<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Transaction</title>

    <!-- Bootstrap core CSS-->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link href="../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="../css/sb-admin.css" rel="stylesheet">
</head>
<body>
<%@include file="structure.jsp"%>
<div id="content-wrapper">

    <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="/banque/monEspace"><fmt:message key="maBanque"/></a>
            </li>
            <li class="breadcrumb-item active">Transaction</li>
        </ol>

        <!-- Page Content -->


            <form action="addTransaction" method="post">
                <div class="form-group">
                    <label for="inputLibelle"><fmt:message key="libelle"/> </label>
                    <input type="text" class="form-control" id="inputLibelle" name="libelle" aria-describedby="libelleHelp" placeholder="<fmt:message key="descripTrans"/> ">
                        <%--<small id="libelleHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>--%>
                </div>
                <div class="form-group">
                    <label for="inputNumber"><fmt:message key="montantmin"/> </label>
                    <input type="number" step="0.01" class="form-control" id="inputNumber" name="montant" aria-describedby="libelleHelp" placeholder="<fmt:message key="ajoutMontant"/> ">
                        <%--<small id="libelleHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>--%>
                </div>
                <div class="form-group">
                    <label for="selectCompteCrediter"><fmt:message key="cptACrediter"/> </label>
                    <select multiple class="form-control" id="selectCompteCrediter" name="compteCrediter">
                        <c:forEach items="${ utilisateurCon.getComptes() }" var="compte" varStatus="status">
                            <option value="${ compte.getId() }">${ compte.getId() }</option>
                        </c:forEach>
                    </select>
                </div>
                <div class="form-group">
                    <label for="selectCompteDebiter"><fmt:message key="cptADebiter"/></label>
                    <select multiple class="form-control" id="selectCompteDebiter" name="compteDebiter">
                        <c:forEach items="${ utilisateurCon.getComptes() }" var="compte" varStatus="status">
                            <option value="${ compte.getId() }">${ compte.getId() }</option>
                        </c:forEach>
                    </select>
                </div>
                <button type="submit" class="btn btn-primary"><fmt:message key="payer"/> </button>
            </form>


        <!-- /.container-fluid -->

        <%@include file="footer.jsp"%>

    </div>
    <script src="../vendor/jquery/jquery.min.js"></script>
    <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="../vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Page level plugin JavaScript-->
    <script src="../vendor/chart.js/Chart.min.js"></script>
    <script src="../vendor/datatables/jquery.dataTables.js"></script>
    <script src="../vendor/datatables/dataTables.bootstrap4.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin.min.js"></script>

    <!-- Demo scripts for this page-->
    <script src="js/demo/datatables-demo.js"></script>
    <script src="js/demo/chart-area-demo.js"></script>

</body>
</html>
