<%--
  Created by IntelliJ IDEA.
  User: romainpetit
  Date: 17/10/2018
  Time: 19:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page pageEncoding="UTF-8" %>
<html>
<head>
    <title><fmt:message key="monCompte"/></title>

    <!-- Bootstrap core CSS-->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link href="../vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
    <link href="../vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="../css/sb-admin.css" rel="stylesheet">

</head>
<body>
<%@include file="structure.jsp"%>

<div id="content-wrapper">

    <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="/banque/monEspace"><fmt:message key="maBanque"/></a>
            </li>
            <li class="breadcrumb-item active"><fmt:message key="comptes"/></li>
        </ol>

        <!-- Page Content -->
        <h1><fmt:message key="comptes"/></h1>
        <hr>
        <p><fmt:message key="bienvenue"/> <c:out value="${ utilisateurCon.getPrenom() }">error name</c:out> !</p>

        <div>
            <p><fmt:message key="comptes"/></p>
        </div>


        <div class="row">
            <c:forEach items="${ Comptes }" var="compte" varStatus="status">
                <div class="col-xl-3 col-sm-6 mb-3">
                    <div class="card text-white bg-primary o-hidden ">
                        <div class="card-body">
                            <div class="mr-5"><fmt:message key="compte"/> N°<c:out value="${ compte.getId() }" /> : <c:out value="${ compte.getSolde() }" /> €</div>
                        </div>

                            <a class="card-footer text-white clearfix small z-1" href="/banque/detailCompte?id=${compte.getId()}">

                                <span class="float-left"><fmt:message key="detail"/></span>
                                <span class="float-right">
                                <i class="fas fa-angle-right"></i>
                                </span>
                            </a>

                        <%--<form class="card-footer text-white clearfix small z-1" action="detailCompte">

                                <input type="hidden" name="id" value="${compte.getId()}" />
                                <input type="submit" name="details" value="<fmt:message key="detail"/>" />
                                &lt;%&ndash;<span class="float-left"><fmt:message key="detail"/></span>
                                <span class="float-right">&ndash;%&gt;
                                <i class="fas fa-angle-right"></i>
                                </span>

                        </form>--%>
                    </div>
                </div>
            </c:forEach>

        </div>
        <div class="row">
            <div class="col-xl">
                <form action="/banque/nouveauCompte">
                    <input type="submit" class="btn btn-primary" value="<fmt:message key="ajouterCompte"/>"/>
                </form>

            </div>
        </div>

    <!-- /.container-fluid -->

    <%@include file="footer.jsp"%>

</div>



<%--<div>
    <div>
        <c:forEach items="${ utilisateurCon.getComptes() }" var="compte" varStatus="status">
            <p>N°<c:out value="${ compte.getId() }" /> : <c:out value="${ compte.getSolde() }" /> €</p>
            <form action="detailCompte">
                <input type="hidden" name="id" value="${compte.getId()}" />
                <input type="submit" name="details" value="<fmt:message key="detail"/>" />
            </form>
        </c:forEach>
    </div>
</div>--%>



</body>
</html>
