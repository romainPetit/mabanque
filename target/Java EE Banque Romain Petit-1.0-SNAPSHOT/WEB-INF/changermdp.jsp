<div class="col-md-6  offset-md-0  toppad">
    <div class="card">
        <div class="card-body">
            <h3 class="card-title"><fmt:message key="changerMdp"/> </h3>
            <c:forEach items="${ infosMdp }" var="info" >
                <h5 style="color: red"><c:out value="${info}"/></h5>
            </c:forEach>
            <h5 style="color: red"><c:out value="${errorMsg}"/></h5>
            <h5 style="color: green"><c:out value="${validMsg}"/></h5>
            <form  method="post">
                <table class="table table-user-information ">
                    <tbody>
                    <tr>
                        <td><fmt:message key="mdpActuel"/> :</td>
                        <td>
                            <input
                                    icon="password-icon"
                                    name="currentPassword"
                                    type="password"
                                    focus
                            />
                        </td>
                    </tr>
                    <tr>
                        <td><fmt:message key="nouveauMdp"/> :</td>
                        <td>
                            <Input
                                    icon="password-icon"
                                    name="newPassword"
                                    type="password"
                            />
                        </td>
                    </tr>
                    <tr>
                        <td>Confirmation:</td>
                        <td>
                            <Input
                                    icon="password-icon"
                                    name="newConfirmPassword"
                                    type="password"
                            />
                        </td>
                    </tr>
                    </tbody>
                </table>
                <input id="id" name="id" type="hidden" value="${utilisateurCon.getLogin()}">
                <input type="submit" class="btn btn-primary btn-block" value="<fmt:message key="modifier"/>">
            </form>
        </div>

    </div>
</div>